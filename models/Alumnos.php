<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumnos".
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellidos
 * @property int $edad
 * @property string $poblacion
 * @property string $telefono
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nombre', 'apellidos', 'edad', 'poblacion', 'telefono'], 'required'],
            [['id', 'edad'], 'integer'],
            [['nombre', 'poblacion'], 'string', 'max' => 50],
            [['apellidos'], 'string', 'max' => 100],
            [['telefono'], 'string', 'max' => 12],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'edad' => 'Edad',
            'poblacion' => 'Poblacion',
            'telefono' => 'Telefono',
        ];
    }
}
